import React from 'react';
import items from '@site/src/js/items';

export default function Product({ productName }) {
    const product = items[productName];

    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                backgroundColor: '#f9f9f9',
                borderRadius: '15px',
                borderWidth: '1px',
                borderStyle: 'solid',
                borderColor: '#ddd',
                padding: '20px',
                boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
                maxWidth: '800px',
                margin: '20px auto',
            }}
        >
            <img
                src={product.imgLink}
                alt={""}
                style={{
                    width: '150px',
                    height: 'auto',
                    marginRight: '20px',
                    borderRadius: '10px',
                    boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.1)',
                }}
            />

            <ul style={{ listStyleType: 'none', padding: 0, margin: 0, marginLeft: 50 }}>
                <li style={{ marginBottom: '10px', color: '#555', fontSize: '16px' }}>
                    <strong>Catégorie :</strong> {product.category}
                </li>
                <li style={{ marginBottom: '10px', color: '#555', fontSize: '16px' }}>
                    <strong>Description :</strong> {product.desc}
                </li>
                <li style={{ marginBottom: '10px', color: '#555', fontWeight: 'bold', fontSize: '16px' }}>
                    <strong>Prix :</strong> {product.prix}
                </li>
                <li>
                    <a
                        href={product.productLink}
                        style={{
                            display: 'inline-block',
                            marginTop: '10px',
                            padding: '8px 12px',
                            backgroundColor: '#1e90ff',
                            color: 'white',
                            borderRadius: '5px',
                            textDecoration: 'none',
                            transition: 'background-color 0.3s ease',
                            fontSize: '14px',
                        }}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Voir le produit
                    </a>
                </li>
            </ul>
        </div>
    );
}
