const items = {
    commentRaterSaVie: {
        desc: "COMMENT RATER SA VIE ? est un guide pour rater sa vie. Des méthodes précises, des schémas explicatifs et des astuces efficaces pour échouer au mieux dans chaque étape de votre vie. Avec ce guide, vous n’avez pas fini de décevoir votre entourage.",
        imgLink: "https://exemplaire-prod.s3.gra.io.cloud.ovh.net/CACHE/images/products/741583d5-0abc-4994-8c7d-877dc742debc/584bdc49462458364b9a8e646b3cadc0.webp",
        category: "Livre",
        prix: "20€",
        productLink: "https://www.exemplaire-editions.fr/librairie/livre/comment-rater-sa-vie"
    },
    wooting: {
        desc: "Un clavier gaming",
        imgLink: "https://wooting-website.ams3.cdn.digitaloceanspaces.com/612ca8920bc3a648125ffac3/6228b91e4f657f1601a79599_W2HE_keyboard_v12-min.png",
        category: "Matériel - PC",
        prix: "200€ (sans livraison)",
        productLink: "https://wooting.io/wooting-two-he"
    },
    screen: {
        desc: "Un écran 1080p@180hz pour remplacer celui que j'ai qui commence a fatiguer",
        imgLink: "https://static.fnac-static.com/multimedia/Images/FR/MDM/49/1a/5a/22682185/3756-1.jpg",
        category: "Matériel - PC",
        prix: "140€",
        productLink: "https://www.fnac.com/Ecran-PC-Gaming-MSI-G27C4-E3-27-Incurve-Full-HD-Noir/a19306581/w-4"
    },
    mako: {
        desc: "Des embouts en plus vu qu'il m'en manque",
        imgLink: "https://cdn.shopify.com/s/files/1/0041/6111/0065/files/2ger4TBIXac5vKqT.jpg?v=1733344101&width=1200",
        category: "Matériel - Autre",
        prix: "40€ (sans livraison)",
        productLink: "https://store.ifixit.fr/products/mako-driver-kit-64-precision-bits"
    },
    icybox: {
        desc: "Un boitier de disque dur",
        imgLink: "https://m.media-amazon.com/images/I/51sie3L-EhL._AC_SX569_.jpg",
        category: "Matériel - PC",
        prix: "19.24€, Livraison gratuite pour +35€",
        productLink: "https://www.amazon.fr/-/en/dp/B0BPMT2DQG"
    }
}

module.exports = items
