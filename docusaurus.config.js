// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Wishlist',
  tagline: 'Cliquer sur le bouton pour accéder a ma liste d\'anniversaire/noël',
  favicon: 'favicon.ico',

  // Set the production url of your site here
  url: 'https:/wishlist.xotak.me/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/wishlist/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'xotak', // Usually your GitHub org/user name.
  projectName: 'wishlist', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'Wishlist',
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Objets',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Plus',
            items: [
              {
                label: 'Source',
                href: 'https://framagit.org/xotak/wishlist',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Xotak. Built with Docusaurus.`,
      },
    }),
};

module.exports = config;
